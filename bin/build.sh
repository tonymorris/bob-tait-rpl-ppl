#!/usr/bin/env bash

set -e
# set -x

CI_PAGES_DOMAIN=${1}
CI_PAGES_URL=${2}
CI_PROJECT_TITLE=${3}
CI_PROJECT_URL=${4}
COMMIT_TIME=${5}
GITLAB_USER_NAME=${6}
GITLAB_USER_EMAIL=${7}
CI_COMMIT_SHA=${8}
CI_PROJECT_VISIBILITY=${9}

export PATH=$HOME/.cabal/bin:$HOME/.ghcup/bin:$PATH

if ! type -p xelatex >/dev/null ; then
  >&2 echo "Missing: xelatex" >&2
fi

if ! type -p pandoc >/dev/null ; then
  >&2 echo "Missing: pandoc" >&2
fi

if ! type -p gs >/dev/null ; then
  >&2 echo "Missing: ghostscript" >&2
fi

if ! type -p convert >/dev/null ; then
  >&2 echo "Missing: imagemagick" >&2
fi

if ! type -p libreoffice >/dev/null ; then
  >&2 echo "Missing: libreoffice" >&2
fi

if ! type -p rsync >/dev/null ; then
  >&2 echo "Missing: rsync" >&2
fi

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
etc_dir=${script_dir}/../etc

mkdir -p ${dist_dir}

function makePage1() {
  pdf_file="${1}"
  pdf_basename=$(basename -- "${pdf_file}")
  pdf_dirname=$(dirname -- "${pdf_file}")
  pdf_filename="${pdf_basename%.*}"

  page1_pdf_dir=${pdf_dirname}
  page1_pdf="${page1_pdf_dir}/${pdf_filename}_page1.pdf"

  mkdir -p "${page1_pdf_dir}"

  gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile="${page1_pdf}" "${pdf_file}"

  page1_pdf_png="${page1_pdf}.png"
  convert ${page1_pdf} ${page1_pdf_png}
}

rsync -aH ${src_dir}/ ${dist_dir}
rsync -aH ${etc_dir}/ ${dist_dir}

vol1_file="${dist_dir}/volume-1.pdf"
vol2_file="${dist_dir}/volume-2.pdf"
complete_file="${dist_dir}/complete.pdf"

function makeVolume1() {
  vol1_dir="${dist_dir}/volume-1"
  mkdir -p "${vol1_dir}"
  pdftk \
    ${vol1_dir}/00-cover.pdf \
    ${vol1_dir}/01-introduction.pdf \
    ${vol1_dir}/00-cover.pdf \
    ${vol1_dir}/01-introduction.pdf \
    ${vol1_dir}/02-about_the_examination.pdf \
    ${vol1_dir}/03-contents.pdf \
    ${vol1_dir}/04-online_exam_preps_for_rpl.pdf \
    ${vol1_dir}/05-part1-radio.pdf \
    ${vol1_dir}/06-part2_aerodynamics-motion-and-control.pdf \
    ${vol1_dir}/07-part3_aerodynamics-vectors_forces_stalling.pdf \
    ${vol1_dir}/08-part4_aerodynamics-design_and_performance.pdf \
    ${vol1_dir}/09-part5_climbing_descending.pdf \
    ${vol1_dir}/10-part6_turning_stalling_spins_spirals.pdf \
    ${vol1_dir}/11-part7_engines_fuel_systems.pdf \
    ${vol1_dir}/12-part8_engine_systems_instruments.pdf \
    ${vol1_dir}/13-part9_basic_aeroplane_performance.pdf \
    ${vol1_dir}/14-part10_human_performance.pdf \
    ${vol1_dir}/15-part11_air_law.pdf \
    ${vol1_dir}/16-part12_basic_navigation.pdf \
    ${vol1_dir}/17-part13_meteorology.pdf \
    ${vol1_dir}/18-pre_solo_test.pdf \
    ${vol1_dir}/19-pre_area_solo_test.pdf \
    ${vol1_dir}/20-pre_rpl_test.pdf \
    ${vol1_dir}/21-answers_pre_area_solo_test.pdf \
    ${vol1_dir}/22-answers_pre_solo_test.pdf \
    ${vol1_dir}/23-answers_pre_rpl_test.pdf \
    ${vol1_dir}/24-index.pdf \
    ${vol1_dir}/25-last-pages.pdf \
    cat \
    output \
    ${vol1_file}
}

function makeVolume2() {
  vol2_dir="${dist_dir}/volume-2"
  mkdir -p "${vol2_dir}"
  pdftk \
    ${vol2_dir}/00-cover.pdf \
    ${vol2_dir}/01-introduction.pdf \
    ${vol2_dir}/02-about_the_examination.pdf \
    ${vol2_dir}/03-contents.pdf \
    ${vol2_dir}/04-part1_performance.pdf \
    ${vol2_dir}/05-part2_meteorology.pdf \
    ${vol2_dir}/06-part3_navigation.pdf \
    ${vol2_dir}/07-part4_air_law.pdf \
    ${vol2_dir}/08-part5_general_knowledge_top_up.pdf \
    ${vol2_dir}/09-part6_casa_sample_ppl_examination_questions.pdf \
    ${vol2_dir}/10-index.pdf \
    ${vol2_dir}/11-last_pages.pdf \
    cat \
    output \
    ${vol2_file}
}

function makeComplete() {
  complete_dir="${dist_dir}"
  mkdir -p "${complete_dir}"
  pdftk \
    ${vol1_file} \
    ${vol2_file} \
    cat \
    output \
    ${complete_file}
}

function makePdfPage1() {
  pdf_files=$(find -L "${dist_dir}" -name '*.pdf' -type f | sort)

  for pdf_file in ${pdf_files}; do
    pdf_file_relative=$(realpath --relative-to=${dist_dir} ${pdf_file})
    pdf_file_relative_dirname=$(dirname ${pdf_file_relative})
    pdf_file_basename=$(basename -- "${pdf_file}")
    pdf_file_filename="${pdf_file_basename%.*}"

    dist_dir_relative=${dist_dir}/${pdf_file_relative_dirname}
    mkdir -p ${dist_dir_relative}

    makePage1 "${dist_dir_relative}/${pdf_file_filename}.pdf"
  done
}

function makeImageFiles() {
  img_files=$(find -L ${dist_dir} -name '*.png' -o -name '*.jpg' -o -name '*.gif' -type f | sort)

  for img_file in ${img_files}; do
    img_file_extension="${img_file##*.}"

    for size in 1200 450 150 100 85
    do
      img_size="${img_file}-${size}.${img_file_extension}"
      convert ${img_file} -resize ${size}x${size} ${img_size}
    done
  done
}

makeVolume1
makeVolume2
makeComplete
makePdfPage1
makeImageFiles
